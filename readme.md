# <center>Cycle Mobile App Front</center>

![Drag Racing](/assets/readme/banner.png)

### React Native ecological-social application.

## Table of contents

1. [Requirements](#requirements)
1. [Libraries](#libraries)
1. [Install](#install)
   - [Additional steps for Android](#install-step-android)
   - [Additional steps for Ios](#install-step-ios)
1. [Run on device - simulator](#run)
   - [Android](#run-android)
   - [Ios](#run-ios)
1. [Connect to Backend](#backend)

# Requirements <a name="requirements"></a>

- [NodeJs](https://nodejs.org/en/)
- [React Native](https://facebook.github.io/react-native/)

        $ npm install -g react-native-cli

`More info for install react native dependencies on MacOs - Windows - Linux` [here](https://facebook.github.io/react-native/docs/getting-started)

Choose React Native CLI Quickstart not Expo !

# Libraries used <a name="libraries"></a>

- Use [React Native](https://facebook.github.io/react-native/) to build the application on Android & Ios devices. [doc](https://facebook.github.io/react-native/docs/getting-started)
- Use [Native Base](https://nativebase.io/) for UI components. [doc](https://docs.nativebase.io/)
- Use [React Navigation](https://reactnavigation.org/) for manage the app navigation [doc](https://reactnavigation.org/docs/en/getting-started.html)
- Use [React Native Gesture Handler](https://kmagiera.github.io/react-native-gesture-handler/) for manage the app navigation [doc](https://kmagiera.github.io/react-native-gesture-handler/docs/getting-started.html)
- Use [React Native Image Crop Picker](https://github.com/ivpusic/react-native-image-crop-picker) image picker with support for camera, video, configurable compression, multiple images and cropping

# Install <a name="install"></a>

- Clone the project and go in

        $ git clone https://gitlab.com/cycle-team/cycle-mobile-app.git  && cd cycle-mobile-app

* Install all dependencies

        $ npm install

* Link libraries

        $ react-native link

IMPORTANT FOR ANDROID <a name="install-step-android"></a>

Use Jetify to convert the app to AndroidX

        $ npx jetify

IMPORTANT FOR IOS <a name="install-step-ios"></a>

Use Cacaopod to manage native libraries

Go to `/ios` folder and install all native libraries from Cocoapods

        $ cd ios
        $ pod install

This commands create:

- `/ios/Pods` folder with all native librairies for Ios build

- `/ios/CycleMobileApp.xcworkspace` with Pods librairies use this folder for build for Ios ! not `CycleMobileApp.xcodeproj`

# Run <a name="run"></a>

## ANDROID <a name="run-android"></a>

        $ react-native run-android

### If error: <a name="run-android-error"></a>

`Android/Ios project not found. Are you sure this is a React Native project?`

    $ rm -rf android              // remove holder build files
    $ react-native eject          // create new android folders
    $ react-native link           // link native dependencies

### IMPORTANT! (React Native Gesture Handler) just for Android ! no need for ios

Use for drawer and native touch functionalities.

You need to add this content in:

`android/app/src/main/java/com/cyclemobileapp/MainActivity.java`

- This three import:

        import com.facebook.react.ReactActivityDelegate;
        import com.facebook.react.ReactRootView;
        import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;

- This methode in MainActivity class:

        @Override
        protected ReactActivityDelegate createReactActivityDelegate() {
            return new ReactActivityDelegate(this, getMainComponentName()) {
            @Override
            protected ReactRootView createRootView() {
            return new RNGestureHandlerEnabledRootView(MainActivity.this);
            }
            };
        }

## IOS <a name="run-ios"></a>

Open `/ios/CycleMobileApp.xcworkspace` with Xcode

![Drag Racing](/assets/readme/ios_xcode_prepare.png)

1. click on project
2. click on project target
3. go to build phase on the top
4. run the build (this command open metro bundler & the application on simulator)

ADD / REMOVE LIBRARIES

If the build fail or error in simulator after build success try this:

- In th Xcode top toolbar

        Product -> Clean build folder

- Go to app target Libraries

        Project -> Targets -> App -> Build Phases -> Link Binary With Libraries

* Remove all libraries except `libPods-CycleMobileApp.a`

* Add the libraries again, look at the picture from above for more info !

---

## Connect to backend <a name="backend"></a>

Go to `/src/config.js` and modify

    apiUrlBackend: string

exemple: 'http(s)://localhost:3000' | 'http(s)://192.168.0.1:3000'
