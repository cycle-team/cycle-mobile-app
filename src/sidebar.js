import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  ProgressBarAndroid,
  TouchableOpacity,
} from 'react-native';
import {Card, CardItem, H2, Icon, Thumbnail} from 'native-base';
import DrawerActions from 'react-navigation-drawer';

export default class CustomSidebarMenu extends Component {
  constructor() {
    super();

    this.items = [
      {
        navOptionThumb: 'animation',
        navOptionName: "Fil d'actualités",
        screenToNavigate: 'Feeds',
      },
      {
        navOptionThumb: 'newspaper',
        navOptionName: 'Articles',
        screenToNavigate: 'News',
      },
      {
        navOptionThumb: 'calendar-range',
        navOptionName: 'Evenements',
        screenToNavigate: '',
      },
      {
        navOptionThumb: 'bullseye-arrow',
        navOptionName: 'Defis',
        screenToNavigate: '',
      },
      {
        navOptionThumb: 'account-group',
        navOptionName: 'Amis',
        screenToNavigate: 'Friends',
      },
      {
        navOptionThumb: 'chat',
        navOptionName: 'Chat',
        screenToNavigate: 'Chat',
      },
      {
        navOptionThumb: 'security',
        navOptionName: 'Authentification',
        screenToNavigate: 'Auth',
      },
    ];
  }

  handleCloseDrawer = () => {
    this.props.navigation.dispatch(DrawerActions.closeDrawer());
  };

  render() {
    // const {navigate} = this.props.navigation;
    return (
      <View style={styles.sideMenuContainer}>
        <Card transparent style={styles.container}>
          <View style={styles.firstItem}>
            <CardItem style={styles.item}>
              <Thumbnail
                medium
                source={{
                  uri: 'https://api.adorable.io/avatars/233',
                }}
              />
            </CardItem>
            <CardItem style={styles.item}>
              <H2>HugoDcrq</H2>
              <Text note>Niveau 2</Text>
            </CardItem>
            <CardItem style={styles.item}>
              <Icon
                name="circle-edit-outline"
                size={25}
                style={{color: '#91C9AA'}}
                type="MaterialCommunityIcons"
                onPress={() => {
                  this.props.navigation.navigate('Profile');
                }}
              />
            </CardItem>
          </View>
        </Card>
        <View style={styles.progress}>
          <Text>Level 4</Text>
          <ProgressBarAndroid
            styleAttr="Horizontal"
            color="#91C9AA"
            indeterminate={false}
            progress={0.7}
            style={{width: '70%'}}
          />
        </View>

        {/*Setting up Navigation Options from option array using loop*/}
        <View
          style={{
            width: '100%',
            backgroundColor: '#91C9AA',
            color: 'white',
            paddingTop: 20,
          }}>
          {this.items.map((item, key) => (
            <TouchableOpacity
              key={key}
              onPress={() => {
                global.currentScreenIndex = key;
                this.props.navigation.navigate(item.screenToNavigate);
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingTop: 10,
                  paddingBottom: 10,
                }}>
                <View style={{marginRight: 10, marginLeft: 20}}>
                  <Icon
                    name={item.navOptionThumb}
                    size={25}
                    type="MaterialCommunityIcons"
                    style={{color: '#fff'}}
                  />
                </View>
                <Text
                  style={{
                    fontSize: 18,
                    color: 'white',
                    fontWeight: '400',
                  }}>
                  {item.navOptionName}
                </Text>
              </View>
            </TouchableOpacity>
          ))}
        </View>
        <View style={{position: 'absolute', bottom: 20, width: '100%'}}>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
              width: '85%',
            }}>
            <Icon
              name="close"
              size={25}
              style={{color: '#fff'}}
              onPress={this.handleCloseDrawer}
            />
            <Icon
              name="settings"
              size={25}
              style={{color: '#fff'}}
              onPress={() => {
                this.props.navigation.navigate('Settings');
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  sideMenuContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: '#91C9AA',
    alignItems: 'center',
    marginBottom: -20,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: 'white',
    marginTop: -5,
    borderRadius: 0,
    width: '99%',
    height: '12%',
  },
  firstItem: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
  },
  item: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  progress: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    height: 30,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingHorizontal: '10%',
    marginTop: -8,
  },
});
