import React from 'react';
import { createSwitchNavigator } from 'react-navigation';

import {createStackNavigator}  from 'react-navigation-stack'
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import {DrawerActions} from 'react-navigation-drawer';
import {TouchableOpacity} from 'react-native';
import {Icon} from 'native-base';

// Custom drawer
import CustomSidebarMenu from './sidebar';

// Tab main pages
import FeedsPage from './pages/tabs/feeds/feeds-page';
import NewsPage from './pages/tabs/news/containers/news-page';
import ProfilePage from './pages/tabs/profile/containers/profile-page';

// Auth pages
import Login from './auth/containers/Login';
import AuthScreen from './auth/containers/AuthScreen';
import CreateAccount from './auth/containers/CreateAccount';

// News pages - components
import NewsAddEditor from './pages/tabs/news/components/news-add-editor';
import HtmlView from './pages/tabs/news/components/news-view';

// Other pages (via drawer)
import SettingsPage from './pages/settings/containers/settings-page';
import FriendsPage from './pages/friends/containers/friends-page';
import ChatListPage from './pages/chat/containers/chat-list-page';

import Example from './pages/exemple';

// Authentification stack navigation
const AuthStack = createSwitchNavigator({
  Auth: {
    screen: AuthScreen,
  },
  SignIn: {
    screen: Login,
  },
  CreateAccount: {
    screen: CreateAccount,
  },
  ForgotPassword: {
    screen: Example,
    navigationOptions: {
      headerTitle: 'Forgot Password',
    },
  },
  ResetPassword: {
    screen: Example,
    navigationOptions: {
      headerTitle: 'Reset Password',
    },
  },
});

/**
const FeedStack = createStackNavigator({
  Feed: {
    screen: Example,
    navigationOptions: {
      headerTitle: 'Feed',
    },
  },
  Details: {
    screen: Example,
    navigationOptions: {
      headerTitle: 'Details',
    },
  },
});

const SearchStack = createStackNavigator({
  Search: {
    screen: Example,
    navigationOptions: {
      headerTitle: 'Search',
    },
  },
  Details: {
    screen: Example,
    navigationOptions: {
      headerTitle: 'Details',
    },
  },
});

const DiscoverStack = createStackNavigator({
  Discover: {
    screen: Example,
    navigationOptions: {
      headerTitle: 'Discover',
    },
  },
  Details: {
    screen: Example,
    navigationOptions: {
      headerTitle: 'Details',
    },
  },
});
 */

export const MainTabs = createBottomTabNavigator(
  {
    Feeds: FeedsPage,
    News: NewsPage,
    Profile: ProfilePage,
    Settings: SettingsPage,
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;

        if (routeName === 'Settings') {
          return (
            <TouchableOpacity
              onPress={() => {
                navigation.dispatch(DrawerActions.openDrawer());
              }}
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100%',
                width: '100%',
              }}>
              <Icon
                name={'menu'}
                active={tintColor === '#91C9AA'}
                color={tintColor}
                type="MaterialCommunityIcons"
              />
            </TouchableOpacity>
          );
        }

        let iconName;

        switch (routeName) {
          case 'Feeds':
            iconName = 'animation';
            break;

          case 'News':
            iconName = 'newspaper';
            break;

          case 'Profile':
            iconName = 'account';

            break;

          case 'Other':
            iconName = 'menu';
            break;

          default:
            break;
        }

        if (routeName !== 'Other') {
          return (
            <Icon
              name={iconName}
              active={tintColor === '#91C9AA'}
              style={{color: tintColor}}
              type="MaterialCommunityIcons"
            />
          );
        }
      },
      tabBarOnPress: ({navigation, defaultHandler}) => {
        if (navigation.state.routeName === 'Other') {
          return null;
        }
        defaultHandler();
      },
    }),
    tabBarOptions: {
      activeTintColor: '#91C9AA',
      inactiveTintColor: 'gray',
      showIcon: true,
      keyboardHidesTabBar: true,
      showLabel: false,
    },
  },
);

const SettingsStack = createStackNavigator({
  SettingsList: {
    screen: Example,
    navigationOptions: {
      headerTitle: 'Settings List',
    },
  },
  Profile: {
    screen: Example,
    navigationOptions: {
      headerTitle: 'Profile',
    },
  },
});

const MainDrawer = createDrawerNavigator(
  {
    MainTabs: MainTabs,
    Settings: SettingsStack,
    Auth: AuthStack,
    Friends: FriendsPage,
    Chat: ChatListPage,
    Editor: NewsAddEditor,
    HtmlView: HtmlView,
  },
  {
    drawerPosition: 'right',
    drawerType: 'front',
    contentComponent: CustomSidebarMenu,
    contentOptions: {
      activeBackgroundColor: 'white',
      activeTintColor: '#91C9AA',
    },
  },
);

const AppModalStack = createStackNavigator(
  {
    App: MainDrawer,
    Promotion1: {
      screen: Example,
    },
  },
  {
    mode: 'modal',
    headerMode: 'none',
  },
);

export const AppNavigator = createSwitchNavigator({
  Loading: {
    screen: MainDrawer,
  },
  Auth: {
    screen: AuthStack,
  },
  App: {
    screen: AppModalStack,
  },
});
