import React, {Component} from 'react';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
  Text,
  Button,
  Icon,
  Title,
} from 'native-base';

import {chats} from '../chat-mock';

export default class ChatListPage extends Component {
  state = {
    chats: chats,
  };

  render() {
    return (
      <Container>
        <Header>
          <Left style={{marginLeft: 5}}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Chat</Title>
          </Body>
          <Right style={{marginRight: 5}}>
            <Button transparent>
              <Icon name="comment-search" type="MaterialCommunityIcons" />
            </Button>
            <Button transparent>
              <Icon name="message-plus" type="MaterialCommunityIcons" />
            </Button>
          </Right>
        </Header>
        <Content>
          <List>
            {this.state.chats.map((chat, index) => {
              return (
                <ListItem avatar key={index}>
                  <Left>
                    <Thumbnail small source={{uri: chat.userImg}} />
                  </Left>
                  <Body>
                    <Text>{chat.userName}</Text>
                    <Text note>{chat.lastChat} ...</Text>
                  </Body>
                  <Right>
                    <Text note>{chat.lastChatTime}</Text>
                  </Right>
                </ListItem>
              );
            })}
          </List>
        </Content>
      </Container>
    );
  }
}
