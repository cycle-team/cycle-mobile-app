export const categories = [
  {
    id: 'energie',
    imageUri:
      'https://fr.cdn.v5.futura-sciences.com/buildsv6/images/square200/0/5/7/057512d687_50140101_tag-categorie-enviro-qr-photov.jpg',
    name: 'Énergie renouvelable'
  },
  {
    id: 'pollution',
    imageUri: 'https://fr.cdn.v5.futura-sciences.com/buildsv6/images/square200/8/e/f/8ef4d13ac9_50140103_tag-categorie-enviro-qr-pol.jpg',
    name: 'Pollution'
  },
  {
    id: 'environment',
    imageUri: 'https://fr.cdn.v5.futura-sciences.com/buildsv6/images/square200/8/1/e/81e42ebe6b_91491_tag-diapo-enviro.jpg',
    name: 'Environnement'
  },
  {
    id: 'sustainable-development',
    imageUri: 'https://fr.cdn.v5.futura-sciences.com/buildsv6/images/square200/4/a/4/4a4210d8e7_50140208_tag-categorie-developpement.jpg',
    name: 'Développement durable'
  },
  {
    id: 'agriculture',
    imageUri: 'https://fr.cdn.v5.futura-sciences.com/buildsv6/images/square200/1/d/7/1d739afbc4_104938_tag-agriculture.jpg',
    name: 'Agriculture'
  },
  {
    id: 'climatology',
    imageUri: 'https://fr.cdn.v5.futura-sciences.com/buildsv6/images/square200/e/c/2/ec2a4cf834_50140207_tag-categorie-climatologie.jpg',
    name: 'Climatologie'
  },
  {
    id: 'water',
    imageUri: 'https://fr.cdn.v5.futura-sciences.com/buildsv6/images/square200/7/f/7/7f7bf057f8_50140091_tag-categorie-enviro-qr-eau.jpg',
    name: 'Eau'
  }
];
