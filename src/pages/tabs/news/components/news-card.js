import React, {Component} from 'react';
import {Image, Dimensions} from 'react-native';
import {
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Left,
  Right,
  Body,
} from 'native-base';
import {Icon} from 'native-base';

const win = Dimensions.get('window');
const ratio = win.width / 600;

export default class NewsCard extends Component {
  getSelectedArticle = () => {
    this.props.openModal(this.props.article.id);
  };

  render() {
    let editButton;
    let publishedButton;

    if (this.props.type === 'user-published') {
      editButton = (
        <Button
          style={{width: '49%'}}
          warning
          block
          small
          onPress={this.getSelectedArticle}>
          <Text>Modifer</Text>
        </Button>
      );

      publishedButton = (
        <Button
          style={{width: '49%'}}
          danger
          block
          small
          onPress={this.getSelectedArticle}>
          <Text>Dépublié</Text>
        </Button>
      );
    } else if (this.props.type === 'user-unpublished') {
      editButton = (
        <Button
          style={{width: '49%'}}
          warning
          block
          small
          onPress={this.getSelectedArticle}>
          <Text>Modifer</Text>
        </Button>
      );

      publishedButton = (
        <Button
          style={{width: '49%'}}
          success
          block
          small
          onPress={this.getSelectedArticle}>
          <Text>Publié</Text>
        </Button>
      );
    }

    return (
      <Card style={{flex: 0, borderRadius: 5, overflow: 'hidden'}}>
        <CardItem>
          <Left>
            <Thumbnail
              small
              source={{
                uri: this.props.article.userImg,
              }}
            />
            <Body>
              <Text style={{fontWeight: 'bold'}}>
                {this.props.article.title}
              </Text>
              <Text note>
                par {this.props.article.userName} le {this.props.article.date}
              </Text>
            </Body>
          </Left>
        </CardItem>
        <CardItem bordered style={{marginTop: -10}}>
          <Body>
            <Image
              source={{
                uri: this.props.article.img,
              }}
              style={{
                flex: 1,
                width: '100%',
                height: 300 * ratio,
                resizeMode: 'cover',
              }}
              resizeMode={'contain'}
            />
            <Text>{this.props.article.description}</Text>
          </Body>
        </CardItem>
        <CardItem bordered>
          <Left>
            <Button transparent textStyle={{color: '#87838B'}}>
              <Icon name={this.props.article.footerIcon} type="FontAwesome" />
              <Text>{this.props.article.footerText}</Text>
            </Button>
          </Left>

          <Right>
            <Button info bordered small onPress={this.getSelectedArticle}>
              <Text>Voir la suite</Text>
            </Button>
          </Right>
        </CardItem>

        {this.props.article.type !== null ? (
          <CardItem style={{marginTop: -10, justifyContent: 'space-between'}}>
            {editButton ? editButton : null}

            {publishedButton ? publishedButton : null}
          </CardItem>
        ) : null}
      </Card>
    );
  }
}
