import React, {Component} from 'react';
import {Button, Segment, Text} from 'native-base';

export default class NewsUserTabs extends Component {
  render() {
    return (
      <Segment>
        <Button
          first
          active={this.props.allFilter}
          onPress={this.props.selectAllArticles}>
          <Text>Tout</Text>
        </Button>
        <Button
          last
          active={this.props.userFillter}
          onPress={this.props.selectUserArticles}>
          <Text>Mes articles</Text>
        </Button>
      </Segment>
    );
  }
}
