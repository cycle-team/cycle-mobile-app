import React, {Component} from 'react';
import {StyleSheet} from 'react-native';

import HTMLText from 'react-htmltext';

const html =
  '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, ' +
  'sed do eiusmod tempor <a href="">incididunt</a> ut labore et dolore magna aliqua.</p>' +
  '<p>Ut <strong>enim ad minim veniam, quis nostrud exercitation</strong> ullamco laboris ' +
  'nisi ut aliquip ex ea commodo consequat. </p>' +
  '<p>Duis aute <code>irure dolor in reprehenderit</code> in voluptate velit esse cillum' +
  'dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, ' +
  'sunt in culpa qui officia deserunt mollit anim id est laborum.</p>';

const baseFontStyle = {
  fontSize: 16,
  fontFamily: 'HelveticaNeue',
  lineHeight: 22,
};
const paragraphStyle = {...baseFontStyle};
const boldStyle = {...baseFontStyle, fontWeight: '500'};
const italicStyle = {...baseFontStyle, fontStyle: 'italic'};
const codeStyle = {...baseFontStyle, fontFamily: 'Menlo'};
const hrefStyle = {...baseFontStyle, fontWeight: '500', color: '#007AFF'};

export default class HtmlView extends Component {
  render() {
    return (
      <HTMLText
        html={html}
        onPress={url => {
          //Code below will be executed when you tap on a link.
          console.log(url);
        }}
      />
    );
  }
}

const styles = {
  styles: StyleSheet.create({
    p: paragraphStyle,
    b: boldStyle,
    strong: boldStyle,
    i: italicStyle,
    em: italicStyle,
    pre: codeStyle,
    code: codeStyle,
    a: hrefStyle,
  }),
};
