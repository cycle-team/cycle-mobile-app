import React, {Component} from 'react';
import {View, Text, Image, TouchableHighlight} from 'react-native';

export default class CategoryCard extends Component {
  selectCategorie = () => {
    this.props.selectCategorie(this.props.categorie.id);
  };

  render() {
    return (
      <TouchableHighlight onPress={this.selectCategorie} underlayColor="white">
        <View
          style={{
            height: 140,
            width: 95,
            marginRight: 5,
            borderWidth: 0.5,
            borderColor: '#dddddd',
            borderRadius: 5,
            overflow: 'hidden',
          }}>
          <View style={{flex: 1}}>
            <Image
              source={{
                uri: this.props.categorie.imageUri,
              }}
              style={{flex: 1, width: null, height: null, resizeMode: 'cover'}}
            />
            <View
              style={{
                position: 'absolute',
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
                height: 160,
                backgroundColor: 'black',
                opacity: 0.3,
              }}
            />
          </View>
          <View
            style={{
              position: 'absolute',
              paddingHorizontal: 2,
              bottom: 5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontWeight: '600', fontSize: 15, color: 'white'}}>
              {this.props.categorie.name}
            </Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}
