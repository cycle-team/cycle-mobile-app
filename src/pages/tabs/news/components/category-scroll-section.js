import React, {Component} from 'react';
import {View, Text, ScrollView} from 'react-native';
import CategoryCard from '../components/category-card';

export default class CategoryScrollSection extends Component {
  selectCategorie = categorieId => {
    this.props.selectCategorie(categorieId);
  };

  render() {
    return (
      <ScrollView scrollEventThrottle={16}>
        <View style={{flex: 1, backgroundColor: 'white', paddingTop: 10}}>
          <Text
            style={{fontSize: 24, fontWeight: '700', paddingHorizontal: 10}}>
            Catégories
          </Text>

          <View
            style={{
              height: 150,
              marginTop: 10,
              marginHorizontal: 10,
            }}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              {this.props.categorys.map(categorie => {
                return (
                  <CategoryCard
                    key={categorie.id}
                    categorie={categorie}
                    selectCategorie={this.selectCategorie}
                  />
                );
              })}
            </ScrollView>
          </View>
        </View>
      </ScrollView>
    );
  }
}
