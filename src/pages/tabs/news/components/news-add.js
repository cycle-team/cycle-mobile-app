import React, {Component} from 'react';

import {
  Button,
  Item,
  Input,
  Label,
  Text,
  View,
  Content,
  Container,
} from 'native-base';

import t from 'tcomb-form-native';
import ImageFactory from 'react-native-image-picker-form';

const Form = t.form.Form;
const DocumentFormStruct = t.struct({
  image: t.String,
});

export default class NewsAdd extends Component {
  constructor() {
    super();

    this.state = {
      value: {},
      options: {
        fields: {
          image: {
            config: {
              title: 'Selectionner une image',
              options: [
                'Prendre une photo',
                'Selectionner depuis la bibliothèque',
                'Annuler',
              ],
              // Used on Android to style BottomSheet
              style: {
                titleFontFamily: 'Roboto',
              },
            },
            error: "Erreur lors du chargement de l'image",
            factory: ImageFactory,
          },
        },
      },
      selectedOption: 'bibliotèque',
    };
  }
  render() {
    const {navigate} = this.props.navigation;
    return (
      <Container>
        <Content>
          <View
            style={{
              display: 'flex',
              flexDirection: 'column',
              width: '100%',
              marginHorizontal: 10,
              marginVertical: 5,
            }}>
            <Form
              style={{width: '80%'}}
              ref={ref => {
                this.form = ref;
              }}
              type={DocumentFormStruct}
              value={this.state.value}
              options={this.state.options}
            />

            <Content>
              <Item stackedLabel>
                <Label>Titre</Label>
                <Input />
              </Item>
              <Item stackedLabel>
                <Label>Courte description</Label>
                <Input />
              </Item>
            </Content>
          </View>

          <Content
            style={{
              marginTop: '10%',
            }}>
            <Button block onPress={() => navigate('Editor')}>
              <Text>Editer</Text>
            </Button>
            <Button block warning onPress={() => navigate('Editor')}>
              <Text>Renitialiser</Text>
            </Button>
            <Button block success onPress={() => navigate('Editor')}>
              <Text>Sauvegarder</Text>
            </Button>
          </Content>
        </Content>
      </Container>
    );
  }
}
