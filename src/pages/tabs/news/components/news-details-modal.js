import React, {Component} from 'react';
import {Modal, Image, Dimensions} from 'react-native';
import {
  Container,
  Header,
  Button,
  Content,
  Right,
  Left,
  Text,
  Thumbnail,
  Body,
  Title,
  Subtitle,
} from 'native-base';

const win = Dimensions.get('window');
const ratio = win.width / 600;

export default class NewsDetailsModal extends Component {
  render() {
    return (
      <Modal
        animationType={'slide'}
        transparent={false}
        visible={this.props.visibility}>
        <Container>
          <Header>
            <Left>
              <Thumbnail
                small
                source={{
                  uri: this.props.article.userImg,
                }}
              />
            </Left>
            <Body>
              <Title style={{fontWeight: 'bold'}}>
                par {this.props.article.userName}
              </Title>
              <Subtitle>le {this.props.article.date}</Subtitle>
            </Body>
            <Right>
              <Button transparent onPress={this.props.closeModal}>
                <Text>Fermer</Text>
              </Button>
            </Right>
          </Header>
          <Content style={{marginBottom: 10}}>
            <Text style={{fontWeight: 'bold', fontSize: 30, margin: 10}}>
              {this.props.article.title}
            </Text>
            <Image
              source={{
                uri: this.props.article.img,
              }}
              style={{
                width: '100%',
                height: 300 * ratio,
                marginTop: 10,
                marginBottom: 10,
              }}
              resizeMode={'contain'}
            />
            <Text style={{margin: 10}}>{this.props.article.content}</Text>
          </Content>
        </Container>
      </Modal>
    );
  }
}
