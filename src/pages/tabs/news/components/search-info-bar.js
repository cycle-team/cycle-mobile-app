import React, {Component} from 'react';
import {View, Badge, Text, Button} from 'native-base';

export default class SearchInfoBar extends Component {
  clearSearch = () => {
    this.props.clearSearch();
  };

  render() {
    return (
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'flex-end',
          marginVertical: 5,
          alignItems: 'center',
        }}>
        <Badge style={{backgroundColor: 'black'}}>
          <Text style={{color: 'white', lineHeight: 30}}>
            {this.props.numberResults} résultats | Recherche:{' '}
            <Text style={{fontWeight: 'bold', color: 'white'}}>
              {this.props.searchValue}
            </Text>
          </Text>
        </Badge>
        <Button
          danger
          small
          onPress={this.clearSearch}
          style={{height: 25, marginLeft: 5, marginRight: 2}}>
          <Text style={{color: 'white', lineHeight: 30, fontSize: 10}}>
            SUPPRIMER
          </Text>
        </Button>
      </View>
    );
  }
}
