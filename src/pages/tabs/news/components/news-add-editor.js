import React, {Component} from 'react';
import {
  KeyboardAvoidingView,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Alert,
} from 'react-native';

import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Button,
  Icon,
  Title,
} from 'native-base';

import RichEditor from 'react-native-pell-rich-editor/src/RichEditor';
import RichToolbar from 'react-native-pell-rich-editor/src/RichToolbar';

import ImagePicker from 'react-native-image-crop-picker';

const initHTML = `<br/>
<center><b>Pell.js Rich Editor</b></center>
<center>React Native</center>
<br/>
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1024px-React-icon.svg.png" ></br></br>
</br></br>
`;

export default class NewsAddEditor extends Component {
  async getHtml() {
    // Get the data here and call the interface to save the data
    let html = await this.richText.getContentHtml();
    console.log(html);

    Alert.alert(
      'Alert Title',
      html,
      [
        {
          text: 'Ask me later',
          onPress: () => console.log('Ask me later pressed'),
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  }

  onPressAddImage = () => {
    if (this.state.selectedOption === 'camera') {
      ImagePicker.openCamera({
        cropping: true,
        width: 500,
        height: 500,
        cropperCircleOverlay: true,
        compressImageMaxWidth: 640,
        compressImageMaxHeight: 480,
        freeStyleCropEnabled: true,
        includeBase64: true,
      })
        .then(image => {
          this.setState({imageModalVisible: false});
          // this.storeUploadedData(item, image);
        })
        .catch(e => {
          console.log(e);
          this.setState({imageModalVisible: false});
        });

      console.log('camera');
    } else {
      ImagePicker.openPicker({
        cropping: true,
        width: 300,
        height: 400,
        cropperCircleOverlay: true,
        freeStyleCropEnabled: true,
        avoidEmptySpaceAroundImage: true,
        includeBase64: true,
      })
        .then(image => {
          this.setState({imageModalVisible: false});

          // this.richText.insertImage(image);
          this.richText.insertImage(`data:${image.mime};base64,${image.data}`);
        })
        .catch(e => console.log(e));
      console.log('gallery');
    }

    // insert URL
    /**
    this.richText.insertImage(
      'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1024px-React-icon.svg.png',
    );
    // insert base64
    // this.richText.insertImage(`data:${image.mime};base64,${image.data}`);
    this.richText.blurContentEditor();
    */
  };

  onHome = () => {
    this.props.navigation.push('index');
  };

  render() {
    let that = this;
    return (
      <Container>
        <Header>
          <Left style={{marginLeft: 5}}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-left" type="MaterialCommunityIcons" />
            </Button>
          </Left>
          <Body>
            <Title>Editeur</Title>
          </Body>
          <Right style={{marginRight: 5}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate('HtmlView')}>
              <Icon name="eye" type="MaterialCommunityIcons" />
            </Button>
            <Button transparent onPress={() => this.getHtml()}>
              <Icon name="content-save" type="MaterialCommunityIcons" />
            </Button>
          </Right>
        </Header>

        <SafeAreaView style={styles.container}>
          <ScrollView style={styles.scroll}>
            <RichEditor
              ref={rf => (that.richText = rf)}
              initialContentHTML={initHTML}
              style={styles.rich}
            />
          </ScrollView>
          <KeyboardAvoidingView>
            <RichToolbar
              style={styles.richBar}
              getEditor={() => that.richText}
              iconTint={'#000033'}
              selectedIconTint={'#2095F2'}
              selectedButtonStyle={{backgroundColor: 'transparent'}}
              onPressAddImage={that.onPressAddImage}
            />
          </KeyboardAvoidingView>
        </SafeAreaView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  nav: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 5,
  },
  rich: {
    minHeight: 300,
    flex: 1,
  },
  richBar: {
    height: 50,
    backgroundColor: '#F5FCFF',
  },
  scroll: {
    backgroundColor: '#ffffff',
  },
});
