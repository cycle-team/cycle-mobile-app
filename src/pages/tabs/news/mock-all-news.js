export const allArticles = [
  {
    id: '1',
    published: true,
    categorie: '',
    userName: 'Meums',
    userImg: 'https://api.adorable.io/avatars/400',
    title: '5 idées pour bricoler écolo à base de recyclage',
    date: '19 août 2019',
    img: 'https://www.darty.com/services/sites/default/files/styles/image_600x300/public/2017-01/reduction_dechets.jpg?itok=1--5iyxI',
    description:
      'Un bon geste pour la planète : récupérez vos pulls usés, ne jetez pas les bocaux et les bouteilles ou encore donnez une nouvelle vie à vos vieux...',
    footerIcon: 'heart',
    footerText: '1,926',
    content: `Un bon geste pour la planète : récupérez vos pulls usés, ne jetez pas les bocaux et les bouteilles ou encore donnez une nouvelle vie à vos vieux vêtements en polaire. Créez un salon de jardin avec vos pneus de voiture, original et naturel.

		Des bouteilles devenues vases

		Le bricolage sur les objets qui n’ont pus d’utilité à la maison vous permettra de leur donner une seconde vie. Au lieu de jeter vos bouteilles, transformez-les en vases décoratifs à utiliser sur un mur d’entrée ou de toilettes par exemple. Pour les réaliser, il vous faut quatre bouteilles en verre de 50 cl, quatre colliers de fixation simples, quatre mini équerres, des sables variés, quatre fleurs artificielles. Vissez les équerres qui supporteront le fond de chacune des bouteilles aux emplacements que vous avez déterminés. Posez la bouteille sur l’équerre et vissez le collier de fixation. Faites la même chose pour chacune des bouteilles. Versez un sable différent dans chacune d’entre elles et déposez une fleur à longue tige dedans.
				
		Donnez de la chaleur à vos décorations

		Récupérez vos vieux pulls pour la décoration de Noël. Utilisez la manche pour entourer la couronne puis décorez-la avec des feuilles en feutrine verte. Découpez deux grandes feuilles et trois plus petites puis fixez-les sur la couronne. Faites une petite boule à l’aide d’un coton que vous aurez peint au préalable en rouge avec de la bombe, collez-la au milieu des feuilles et votre couronne est prête. Servez-vous également des pulls usés pour habiller des bougeoirs. Choisissez des bocaux de taille moyenne et mettez-y des bougies flottantes. Puis habillez l’extérieur avec le pull, mesurez le pourtour et découpez le morceau puis faites un rond pour le fond et assemblez le tout.
		
		Les bocaux et bouteilles pour décorer votre intérieur

		Les bocaux et bouteilles serviront de décoration dans votre maison. Il vous suffit de les peindre avec des couleurs tendance ou en harmonie avec la pièce où vous souhaitez les mettre. Pour les bocaux, vous pouvez vous en servir comme pot à crayons également. Optez pour des bombes pour les peindre, c’est plus facile à manipuler. Après les avoir peints, utilisez des masking tape pour faire des petits motifs : étoiles, papillons ou petits cœurs. Si vous souhaitez deux ou trois couleurs sur les bocaux et bouteilles, cachez la partie à ne pas peindre avec du papier que vous maintiendrez avec du scotch. Décorez-les selon votre inspiration.
		
		Un dessous de plat en bouchons

		À partir d’aujourd’hui, ne jetez plus vos bouchons en liège. Ils supportent bien la chaleur et feront une belle décoration sur votre table. Pour fabriquer votre dessous de plat, découpez une planche médium de 20X20cm puis poncez-la. Prenez une vingtaine de bouchons, découpez suivant la longueur et à l’aide d’un pistolet à colle, fixez la face plate sur la planche. Vous pouvez alterner la disposition des bouchons, à l’horizontale ou à la verticale, selon vos préférences. Laissez bien sécher la colle avant d’utiliser votre dessous de plat pas cher et écolo.
				
		Une seconde vie pour les vêtements polaires

		Après de bons et loyaux services, votre pull polaire n’est pas obligé de prendre la direction des oubliettes. Vous pouvez le garder pour d’autres utilisations, comme le transformer en lingette. Pour cela, découpez deux petits rectangles et rassemblez les morceaux en cousant sur le côté. Votre pull polaire recyclé peut également devenir une enveloppe de bouillotte. Prenez les mesures de la bouillotte, découpez deux morceaux et rassemblez-les. Veillez à laisser une ouverture assez large sur le haut pour faire glisser la bouillotte.`
  },
  {
    id: '2',
    published: true,
    categorie: '',
    userName: 'HugoDcrq',
    userImg: 'https://api.adorable.io/avatars/233',
    title: `3 idées pour économiser de l'eau à la maison`,
    date: '18 août 2019',
    img: 'http://montletroit.fr/wp-content/uploads/goutte-eau600x300.jpg',
    description:
      'L’eau n’est pas inépuisable. Eviter de la gaspiller est primordial voire vital. A savoir déjà que sur la planète, plus de 90% de l’eau est...',
    footerIcon: 'heart',
    footerText: '1,926',
    content: `L’eau n’est pas inépuisable. Eviter de la gaspiller est primordial voire vital. A savoir déjà que sur la planète, plus de 90% de l’eau est sous forme salée dans les océans et les mers. L’eau douce est beaucoup moins abondante et ne représente que 0,65%. En plus des besoins des hommes qui ne cessent d’augmenter, les écosystèmes naturels ont également besoin d’eau.

		Une sur-utilisation et une surexploitation des nappes souterraines et des cours d’eau entraînent de gros dégâts et pas seulement dans les milieux aquatiques. Avec le réchauffement planétaire tendant à réduire les précipitations, la planète risque bien de manquer d’eau d’ici peu. Economiser cette précieuse ressource doit devenir une préoccupation de chaque individu. Pour y arriver, appliquer certaines techniques devient indispensable.
		
		Cibler et colmater les fuites

		En France, les fuites constituent 30 à 40% des pertes d’eau. En la matière, les chiffres sont presque effrayants. Une chasse d’eau défaillante qui perd de l’eau peut écouler jusqu’à 300 litres quotidiennement, soit la consommation par jour d’un ménage composé de quatre personnes. Idem : un robinet qui laisse passer des gouttes d’eau peut faire perdre jusqu’à 120 litres.
		
		Pour remédier à ce problème, il existe heureusement certaines solutions. Vous pouvez par exemple installer un dispositif qui coupe automatiquement l’alimentation en eau dès qu’il détecte des anomalies dans votre consommation. Cet appareil doit être placé après le compteur. Sinon, vous pouvez réaliser le test vous-même. La nuit, avant d’aller dormir, relevez le compteur. Le matin, vérifiez celui-ci. S’il a fonctionné durant la nuit, cela signifie qu’il y a probablement une fuite quelque part.
		
		Bien entendu, pour que cette astuce fonctionne, il faut éviter de faire marcher vos robinets ou vos toilettes la nuit du test. Par ailleurs, à moins que vous ne disposiez de compétences poussées en matière de plomberie, il est préférable de faire appel à un professionnel pour qu’il détecte puis colmate la fuite.
		
		Des WC moins gourmands en eau

		On n’en a pas toujours conscience mais les toilettes constituent le deuxième poste de dépense en consommation d’eau dans une maison, d’où l’intérêt d’installer par exemple des dispositifs hydro-économes. En principe, un équipement classique consomme jusqu’à 10 litres d’eau à chaque utilisation.
		
		L’idéal est d’investir directement dans l’installation d’une chasse d’eau à double commande de 3 à 6 litres. Si vous vivez dans une maison individuelle, envisagez de mettre en place un système de récupération d’eau de pluie qui servira spécialement dans les toilettes et utilisable également pour vos travaux d’arrosage. D’autres idées sont à trouver sur Focus Maison.
		
		Solutions anti-gaspillage dans la salle de bain

		Dans la salle de bain, installer des douchettes économiques ou limiteurs de pression est une excellente idée. Ces équipements ont pour objectif de réduire la consommation en eau en ne changeant pas le confort d’utilisation lors de vos douches puisque vous bénéficiez de la même sensation même si la quantité d’eau diminue.
		
		Un système d’aspiration d’air est utilisé sur ces dispositifs, ce qui va contribuer à la variation du nombre et de la taille des gouttes d’eau. Evitez néanmoins l’usage des systèmes à effet Venturi car ils ont un impact sur la santé des poumons à cause de taille des gouttes prenant la forme de particules COV (composés organiques volatils).`
  },
  {
    id: '3',
    published: true,
    categorie: '',
    userName: 'Bigby',
    userImg: 'https://api.adorable.io/avatars/245',
    title: '5 trucs écolos à pratiquer au jardin',
    date: '19 août 2019',
    img: 'http://www.bienchezsoi.net/img/photos/univers-jardin-fs.jpg',
    description:
      'Depuis quelques années, le jardinage écologique n’est plus une simple tendance, mais s’apparente à un véritable mode de culture pour de...',
    footerIcon: 'heart',
    footerText: '1,926',
    content: `Depuis quelques années, le jardinage écologique n’est plus une simple tendance, mais s’apparente à un véritable mode de culture pour de nombreuses personnes. Voici quelques gestes à adopter.

		Une consommation d’eau très contrôlée

		Si vous recherchez des trucs et astuces sur le jardinage, c’est ici : https://jardinage.lemonde.fr/dossiers-cat-1-trucs-astuces.html. En parcourant les articles publiés sur ce site, vous saurez quelles plantes ont plus besoin d’arrosage que d’autres. En effet, avant toute chose, un jardinier écolo se doit d’économiser l’eau. Pour ce faire, les séances seront de préférable à programmer en début de matinée ou encore en fin de soirée, lorsque le soleil n’est pas encore à son zénith. Pour éviter le gaspillage, au lieu des feuilles des plantes, n’arrosez que le sol pour bien atteindre les racines. Et surtout, investissez dans un récupérateur ou un collecteur d’eau de pluie. Renseignez-vous auprès de votre mairie : certaines d’entre elles en fournissent gratuitement.
		
		L’importance du paillage
		
		Le paillage consiste à couvrir les pieds des plantes et des plates-bandes de matières d’origine végétale comme les tontes de gazon ou les feuilles mortes. Destiné dans un premier temps à surtout garder l’humidité au pied des plantations, le paillis empêche aussi l’envahissement des mauvaises herbes et protége le terrain de l’érosion.
		
		Par ailleurs, l’humidité constante du sol favorise la prolifération des insectes et des petites bêtes utiles à la terre. Mais le paillage permet aussi d’améliorer la fertilité du sol. À noter que des paillis prêt-à-l’emploi sont disponibles sur le marché, mais éviter les écorces de pin qui acidifient le sol. Les matériaux synthétiques seront bien-sûr à éviter.
		
		Des fertilisants 100 % naturels

		Jardinier professionnel ou seulement amateur, qui ne rêve pas de massifs magnifiquement fleuris ou de beaux légumes appétissants ? Pour obtenir satisfaction, certains n’hésitent alors pas à recourir aux fertilisants chimiques. Pourtant, l’utilisation de ces produits aura des conséquences négatives, voire même néfastes tant sur l’environnement, la nappe phréatique, la faune du jardin que sur les habitants du lieu.
		
		À cet effet, les engrais bio seront à privilégier comme le fumier de vaches, de chevaux ou de moutons ou encore les mélanges de sans séché, corne broyée, etc... Sinon, en alternative, il y a aussi les engrais maison du type marc de café ou feuilles d’orties (pleins de vidéo à découvrir à ce sujet sur Youtube).
		
		Des pesticides écologiques

		La même attitude est également à adopter pour lutter contre les nuisibles et les maladies qui envahissent nos plantes. À bout d’impatience ou désirant tout simplement en finir définitivement avec les taupes, limaces et autres, on peut être tenté de recourir à des solutions radicales que sont les insecticides chimiques. Ce serait une erreur, surtout que fort heureusement, les méthodes naturelles, ni nocives, ni dangereuses, ont déjà fait aussi leurs preuves. Ainsi, pour venir à bout des chenilles, par exemple, il y a le choix entre planter du tournesol, ou encore du romarin à proximité des plantes concernées.
		
		Le compostage domestique

		Enfin, même si le fertilisant naturel se trouve de nos jours très facilement, rien ne vaut des engrais faits maison. Et c’est là qu’intervient le compostage domestique. De plus la fabrication de compost est très facile, mais surtout à la portée de main de tout le monde, puisqu’il suffit de recycler les déchets issus de la cuisine et des végétaux en provenance du jardin. Pour ne pas être incommodé par les odeurs de pourrissement et de fermentation, installez le bac de compostage dans un endroit éloigné des lieux d’habitation. Sinon, optez pour le lombricompostage, plus facile pour les terrasses et petits jardins.`
  },
  {
    id: '4',
    published: true,
    categorie: '',
    userName: 'Bigby',
    userImg: 'https://api.adorable.io/avatars/245',
    title: 'Quel mode de transport écologique et durable ?',
    date: '18 août 2019',
    img: 'http://www.etvonweb.be/wp-content/uploads/2015/06/velosophe_yojiro_oshima_web1.jpg',
    description:
      'Saviez-vous que près de 25 % des gaz à effet de serre sont issus de nos moyens de transport ? À l’heure où la destruction de la couche...',
    footerIcon: 'heart',
    footerText: '1,926',
    content: `Saviez-vous que près de 25 % des gaz à effet de serre sont issus de nos moyens de transport ? À l’heure où la destruction de la couche d’ozone se fait à vitesse grand V, le moment est plus que jamais à une prise de conscience collective notamment sur nos moyens de déplacement. Convertissez-vous au mode de transport durable et écolo !

		Le transport individuel

		Pareillement à nombreux d’entre vous, j’ai eu beaucoup de mal à me passer de ma voiture à essence. En effet, je l’utilisais dans le moindre de mes déplacements. Les rendez-vous professionnels, les virées shopping avec mes copines, les transports des enfants pour leurs activités, les sorties familiales… Lorsque je devais me rendre quelque part, je sortais ma voiture du garage. Cependant, suite à des reportages et des documentaires que j’ai regardés sur l’environnement, j’ai pris la très ferme décision de changer mes habitudes.
		
		Bien sûr, je ne vais pas complètement délaisser mon véhicule. Mais par contre, j’ai limité son usage. Ainsi, la première chose que j’ai faite est de me mettre au cyclisme urbain. Et pour joindre l’acte à la parole, j’ai fait l’achat de deux vélos sur Hollande Bikes, pour la famille. Le premier est un modèle pliant de ville. Je m’en sers surtout pour les trajets de courte distance, et mon mari me le chipe régulièrement. Le deuxième est un tripoteur électrique.
		
		Ce vélo de transport m’est d’une grande nécessité lorsque je fais mes courses au centre commercial, ou pour emmener mon petit dernier au parc le samedi après-midi. En outre, avec le cyclisme, je me suis également mise à la marche. Je fais beaucoup de trajets à pied dans mon quartier. Et pour mon plus grand bonheur, les membres de ma famille s’y sont également mis. Enfin comme je ne peux pas faire une impasse sur les voitures, je compte investir très prochainement dans une petite voiture électrique ou pourquoi pas une hybride.
		
		Le transport collectif

		Mon autre astuce pour utiliser le moins possible ma voiture est de prendre les transports en commun. Comme toutes les grandes villes françaises, la mienne est dotée d’excellents réseaux. Cette option présente en effet de nombreux avantages. Avant toute chose, en utilisant ce mode de déplacement, je fais des économies très intéressantes puisque le coût des tickets de bus est très abordable par rapport au prix de l’essence.
				
		Avec tout cela, en roulant occasionnellement avec ma voiture, je provoque moins d’usure sur le véhicule et je baisse inévitablement les frais d’entretien et de réparation qui lui sont attachés. Et fini également le stress et les frustrations liés aux embouteillages. En prenant le bus, j’arrive sur mon lieu de travail détendue et zen, même si le temps de trajet et parfois un peu plus long. L’essentiel de cette démarche est son impact écologique important.
		
		En effet, si chacun d’entre nous privilégie de temps en temps les transports en commun, moins de voitures circuleront et il y aura moins d’émission de CO2. Vous pourrez m’objecter que les autobus carburent tout de même aux énergies fossilisées, donc ils sont polluants : certes, mais la pollution émise par rapport au nombre de gens transportés est amoindrie, d’une part et d’autre part, de plus en plus de bus fonctionnent au gaz ou sont hybrides. Quant aux tramways et aux TGV, ils sont équipés de moteurs électriques. Sinon, vous avez aussi l’auto-partage et le covoiturage. Ces pratiques comptent déjà de nombreux adeptes en France.`
  },
  {
    id: '5',
    published: true,
    categorie: '',
    userName: 'HugoDcrq',
    userImg: 'https://api.adorable.io/avatars/233',
    title: 'Le lombricompostage, idéal en ville',
    date: '19 août 2019',
    img: 'https://www.jardinier-amateur.fr/img/reportages/p/h/photo-323-1513-l0-h0.jpeg',
    description:
      'Tout le monde connait le compost mais beaucoup moins de gens savent ce que recouvre le lombricompostage. On pourrait résumer à dire qu’il...',
    footerIcon: 'heart',
    footerText: '1,926',
    content: `Tout le monde connait le compost mais beaucoup moins de gens savent ce que recouvre le lombricompostage. On pourrait résumer à dire qu’il s’agit de la même chose à une plus petite échelle mais ce serait insuffisant et incomplet.

		Mise en place d’un lombricomposteur
		
		Le principe du lombricompostage est de produire un terreau (riche en azote, phosphore, potassium, calcium et magnésium) permettant de fertiliser votre jardin ou vos pots de fleurs à partir des déchets organiques qui auront été transformés par des vers rouges. Ces derniers se nourrissent des épluchures mises dans le lombricomposteur et vont rejeter ce qui fera ce terreau. Je vous rassure, ils rejettent beaucoup moins, en volume, que ce qu’ils ont englouti : environ 5 fois moins. Et puis, ce terreau n’a pas d’odeur, ne craignez pas les odeurs pestilentielles !
		
		Pour mettre en place votre lombricomposteur, vous avez deux solutions au choix : l’acheter ou le fabriquer vous-même. Pour l’achat, des fabricants en sont devenus spécialistes tels que Ecoworms, Lombribox ou Can-o-worms, par exemple. Ce sont d’ailleurs aussi auprès d’eux que vous vous procurerez les vers adaptés : Eisenia Fetida, Eisenia andrei ou Eisenia hortensis. Si vous êtes un tantinet bricoleur, sa fabrication n’est pas très compliquée. Vous trouverez différents tutoriels d’explication sur internet à partir de boites de polystyrène ou de bacs plastique empilables.
		
		Vous pouvez le placer sur un balcon, dans votre cave, sous un abri, dans une buanderie, voire même dans une cuisine : attention car les vers craignent le gel et les grosses chaleurs. Ils sont au mieux de leur forme entre 15 et 30C°.
		
		Quelles matières mettre dans le lombricomposteur ?

		Les vers mangent à peu près tous vos restes d’alimentation, vos épluchures de fruits et légumes, les restes de pain émietté, les coquilles d’oeuf broyées qui équilibreront le Ph, les sachets de thé, le marc de café, etc. Il est à noter quelques exceptions à ne pas mettre au lombricomposteur : la viande, l’ensemble des produits laitiers, les pâtes et le riz (moisiront et attireront les mouches), les restes d’épices et d’herbes aromatiques, les agrumes, l’ail et la rhubarbe (effet vermifuge), l’oignon et le poireau (trop acide).
		
		En dehors des déchets alimentaires, d’autres résidus peuvent être mis tels que les cheveux (riches en azote), le papier, les mouchoirs en papier, les boites d’oeufs ou encore quelques feuilles d’arbres (apport carboné).
		
		L’arrivée de mouches, le manque d’humidité, les odeurs nauséabondes,une fermentation anaérobie sont les quelques petits soucis que vous pouvez rencontrer dans les premiers temps. Mais au bout de 3 mois, vous aurez atteint votre rythme de croisière et votre lombricomposteur fonctionnera à merveille vous permettant de commencer à récupérer le terreau du premier bac. Un engrais liquide très concentré est aussi à récupérer dans la cuve du fond du lombricomposteur : il devra être dilué dans 10 volumes d’eau pour arroser vos plantes.
		
		Où que vous habitiez, quelle que soit la taille de votre foyer, faites l’essai. Et si vous avez des enfants, ce sera à la fois ludique et très instructif pour eux, en matière de recyclage et d’environnement.`
  },
  {
    id: '6',
    published: true,
    categorie: 'energie',
    userName: 'HugoDcrq',
    userImg: 'https://api.adorable.io/avatars/233',
    title: `Le réchauffement climatique menace la production d'énergie solaire`,
    date: '19 août 2019',
    img:
      'https://fr.cdn.v5.futura-sciences.com/buildsv6/images/wide1440/2/a/3/2a3303d0d7_50154012_panneau-solaire-rechauffement-kampan-fotolia.jpg',
    description: `Contre le réchauffement climatique, les experts s'accordent à dire qu'il est nécessaire de développer les énergies renouvelables comme le solaire. Sauf que ces énergies renouvelables sont elles-mêmes lourdement impactées... par le réchauffement....`,
    footerIcon: 'heart',
    footerText: '1,926',
    content: `Les panneaux solaires n'apprécient pas du tout les grosses chaleurs. Lorsque les photons traversent les cellules photovoltaïques, elles arrachent des électrons aux atomes de silicium créant alors un « trou » dans le matériau semi-conducteur. Un facteur important de l'efficacité d'un panneau solaire est la vitesse à laquelle les électrons se recombinent avec les trous. Or, ce taux de recombinaison est très sensible à la température : plus il fait chaud, plus il est élevé, ce qui diminue le rendement. On estime ainsi qu'au-delà de 25 °C, une augmentation de 1 °C aboutit à une baisse de production de 0,45 %. Sous des températures ambiantes de 35 °C, les cellules peuvent atteindre 80 °C en surface et perdre jusque 30 % de leur rendement. Lors du pic de consommation d'électricité dû à la canicule le 24 juillet 2019, le parc solaire français n'a ainsi pu produire que 6.100 MW, contre une puissance théorique maximale de 8.612 MW, soit à peine 11 % de la production nationale selon le gestionnaire du Réseau de Transport d'Électricité (RTE).

    Des pertes de rendement pouvant aller jusqu’à 50 kWh par kW installé

    Dans une étude publiée sur la plateforme scientifique arXiv, Ian Peters et Tonio Buonassisi, chercheurs au Massachusetts Institute of Technology (MIT), ont modélisé les effets d'un réchauffement global de 1,8 °C à l'échelle de notre Planète. D'après leurs calculs, le rendement des panneaux photovoltaïques devrait chuter en moyenne de 15 kWh par kW installé. Mais certaines régions, comme le sud des États-Unis, l'Afrique du Sud ou l'Asie centrale seront encore plus touchées, avec des baisses pouvant aller jusqu'à 50 kWh.
    
    Les effets pervers d’une baisse des émissions polluantes

    La température est loin d'être le seul facteur influençant la production solaire. L'humidité, le vent ou l'ensoleillement ont un rôle encore plus déterminant. D'après une étude parue dans le magazine Nature en 2015, la production solaire en Europe pourrait baisser de 10 à 12 % dans les régions d'Europe du Nord en raison de la baisse d'ensoleillement, notamment en hiver. Une autre étude du centre de recherche de la Commission européenne (JRC) met en garde contre les effets pervers d'une réduction la pollution. D'un côté, la baisse locale du nombre de particules contribue à améliorer l'ensoleillement. Mais cet effet est largement compensé par l'augmentation de la circulation atmosphérique produisant une couverture nuageuse plus importante, indiquent les auteurs. Là encore, il y a aurait des gagnants et des perdants : l'Afrique du Nord et l'Europe de l'Est verraient leur production baisser de 7 % tandis que la Méditerranée et l'Europe de l'Ouest pourraient connaitre une hausse de 10 %. Cette diminution des aérosols est aussi susceptible d'entraîner une augmentation des vagues de chaleur... qui réduit le rendement du photovoltaïque.
        
    Doper les cellules solaires pour compenser les effets du réchauffement

    Toutes ces prévisions ne prennent pas non plus en compte les améliorations technologiques. « Une percée dans les sciences des matériaux pourraient modifier considérablement la sensibilité des cellules photovoltaïques à la température », avancent Ian Peters et Tonio Buonassisi. Les cellules au tellurure de cadmium, par exemple, ne perdent que 3 % de leur puissance par tranche de 10 °C supplémentaire, contre 5 % pour les cellules cristallines. D'autres recherches visent à exploiter un spectre plus large du rayonnement grâce à des nanotubes de carbone ou des nanoparticules « réverbérantes  ».
    
    S'il est bien difficile d'évaluer les effets globaux du changement climatique sur la production d'énergie solaire, une chose est certaine : la variabilité des performances va s'accentuer partout dans le monde, rendant encore plus complexe la gestion du réseau.`
  }
];
