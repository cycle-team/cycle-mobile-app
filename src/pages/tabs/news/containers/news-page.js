import React, {Component} from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Left,
  Right,
  Body,
  Text,
  Tab,
  Tabs,
  TabHeading,
  Button,
  Icon,
  Item,
  Input,
} from 'native-base';
// components
import NewsCard from '../components/news-card';
import NewsHeaderSegment from '../components/news-header-segment';
import NewsDetailsModal from '../components/news-details-modal';
import NewsAdd from '../components/news-add';
import CategoryScrollSection from '../components/category-scroll-section';
import SearchInfoBar from '../components/search-info-bar';

// fake data via mock
import {allArticles} from '../mock-all-news';
import {userArticles} from '../mock-user-news';
import {categories} from '../mock-categories';

class NewsPage extends Component {
  static navigationOptions = {
    title: 'News',
  };

  constructor(props) {
    super(props);

    this.state = {
      categories: {data: categories, selected: ''},
      articleFilter: {
        all: true,
        user: false,
      },
      articles: allArticles,
      modalVisible: false,
      modalArticle: allArticles[0],
      searchBar: {
        active: false,
        value: '',
      },
    };
  }

  handlerSelectAllArticles = () => {
    this.setState({
      articleFilter: {
        all: true,
        user: false,
      },
      articles: allArticles,
      searchBar: {active: false, value: ''},
    });
  };

  handlerSelectUserArticles = () => {
    this.setState({
      articleFilter: {
        all: false,
        user: true,
      },
      articles: userArticles,
      searchBar: {active: false, value: ''},
    });
  };

  handlerShowDetailsModal = articleId => {
    const article = this.state.articles.find(
      article => article.id === articleId,
    );
    this.setState({modalVisible: true, modalArticle: article});
  };

  handlerHideDetailsModal = () => {
    this.setState({modalVisible: false});
  };

  handlerShowSearchBar = () => {
    this.setState({
      searchBar: {active: true, value: this.state.searchBar.value},
    });
  };

  handlerHideSearchBar = () => {
    this.setState({
      searchBar: {active: false, value: this.state.searchBar.value},
    });
  };

  searchArticles = value => {
    if (value.length > 0) {
      const articlesFiltered = this.state.articles.filter(article => {
        const itemData = article.title
          ? article.title.toUpperCase()
          : ''.toUpperCase();
        const textData = value.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      this.setState({
        articles: articlesFiltered,
        searchBar: {active: true, value: value},
      });
    } else {
      this.setState({
        articles: allArticles,
        searchBar: {active: true, value: value},
      });
    }
  };

  clearSearch = () => {
    this.setState({
      articles: allArticles,
      searchBar: {active: false, value: ''},
    });
  };

  selectCategorie = categorie => {
    const articlesFiltered = allArticles.filter(article => {
      const itemData = article.categorie
        ? article.categorie.toUpperCase()
        : ''.toUpperCase();
      const textData = categorie.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      categories: {data: [...this.state.categories.data], selected: categorie},
      articles: articlesFiltered,
    });
  };

  getArticle = article => {
    if (article.published) {
      return (
        <NewsCard
          key={article.id}
          article={article}
          openModal={this.handlerShowDetailsModal}
        />
      );
    }
  };

  render() {
    // const {navigate} = this.props.navigation;
    return (
      <Container>
        {!this.state.searchBar.active ? (
          <Header>
            <Left style={{marginLeft: 5}}>
              <Title>Articles</Title>
            </Left>
            <Body style={{marginLeft: 30}}>
              <NewsHeaderSegment
                allFilter={this.state.articleFilter.all}
                userFillter={this.state.articleFilter.user}
                selectAllArticles={this.handlerSelectAllArticles}
                selectUserArticles={this.handlerSelectUserArticles}
              />
            </Body>
            <Right style={{marginRight: 5}}>
              <Button transparent onPress={this.handlerShowSearchBar}>
                <Icon name="search" />
              </Button>
            </Right>
          </Header>
        ) : (
          <Header searchBar>
            <Item>
              <Icon name="ios-search" />
              <Input
                placeholder="Search"
                autoFocus={true}
                onChangeText={text => this.searchArticles(text)}
                onSubmitEditing={this.handlerHideSearchBar}
                value={this.state.searchBar.value}
              />
              <Icon
                onPress={this.handlerHideSearchBar}
                name="ios-close"
                style={{fontSize: 40}}
              />
            </Item>
            <Button transparent>
              <Text>Search</Text>
            </Button>
          </Header>
        )}

        {this.state.articleFilter.user ? (
          <Tabs>
            <Tab
              heading={
                <TabHeading>
                  <Icon
                    name="check-circle-outline"
                    type="MaterialCommunityIcons"
                    style={{fontSize: 20}}
                  />
                  <Text>Publié</Text>
                </TabHeading>
              }>
              <Content style={{padding: 5}}>
                {this.state.searchBar.value &&
                this.state.searchBar.value.length > 0 ? (
                  <SearchInfoBar
                    numberResults={this.state.articles.length}
                    searchValue={this.state.searchBar.value}
                    clearSearch={this.clearSearch}
                  />
                ) : null}

                {this.state.articles.map(article => {
                  return this.getArticle(article);
                })}
              </Content>
            </Tab>
            <Tab
              heading={
                <TabHeading>
                  <Icon
                    name="close-circle-outline"
                    type="MaterialCommunityIcons"
                    style={{fontSize: 20}}
                  />
                  <Text>Non publié</Text>
                </TabHeading>
              }>
              <Content style={{padding: 5}}>
                {this.state.searchBar.value &&
                this.state.searchBar.value.length > 0 ? (
                  <SearchInfoBar
                    numberResults={this.state.articles.length}
                    searchValue={this.state.searchBar.value}
                    clearSearch={this.clearSearch}
                  />
                ) : null}

                {this.state.articles.map(article => {
                  return this.getArticle(article);
                })}
              </Content>
            </Tab>
            <Tab
              heading={
                <TabHeading>
                  <Icon
                    name="plus-circle-outline"
                    type="MaterialCommunityIcons"
                    style={{fontSize: 20}}
                  />
                  <Text>Créer</Text>
                </TabHeading>
              }>
              <NewsAdd navigation={this.props.navigation} />
            </Tab>
          </Tabs>
        ) : (
          <Content>
            {this.state.searchBar.value &&
            this.state.searchBar.value.length > 0 ? null : (
              <CategoryScrollSection
                categorys={this.state.categories.data}
                selectCategorie={this.selectCategorie}
              />
            )}

            <Content style={{padding: 5}}>
              {this.state.searchBar.value &&
              this.state.searchBar.value.length > 0 ? (
                <SearchInfoBar
                  numberResults={this.state.articles.length}
                  searchValue={this.state.searchBar.value}
                  clearSearch={this.clearSearch}
                />
              ) : (
                <Text
                  style={{
                    fontSize: 24,
                    fontWeight: '700',
                    marginBottom: 10,
                    paddingLeft: 5,
                  }}>
                  Les dernières publications
                </Text>
              )}
              {this.state.articles.length > 0 ? (
                this.state.articles.map(article => {
                  return this.getArticle(article);
                })
              ) : (
                <Text>Aucuns articles</Text>
              )}
            </Content>
          </Content>
        )}

        <NewsDetailsModal
          visibility={this.state.modalVisible}
          article={this.state.modalArticle}
          closeModal={this.handlerHideDetailsModal}
        />
      </Container>
    );
  }
}

export default NewsPage;
