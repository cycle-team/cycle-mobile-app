export const userArticles = [
  {
    id: '2',
    userName: 'HugoDcrq',
    published: true,
    userImg: 'https://api.adorable.io/avatars/233',
    title: `3 idées pour économiser de l'eau à la maison`,
    date: '18 août 2019',
    img: 'http://montletroit.fr/wp-content/uploads/goutte-eau600x300.jpg',
    description:
      'L’eau n’est pas inépuisable. Eviter de la gaspiller est primordial voire vital. A savoir déjà que sur la planète, plus de 90% de l’eau est...',
    footerIcon: 'heart',
    footerText: '1,926',
    content: `L’eau n’est pas inépuisable. Eviter de la gaspiller est primordial voire vital. A savoir déjà que sur la planète, plus de 90% de l’eau est sous forme salée dans les océans et les mers. L’eau douce est beaucoup moins abondante et ne représente que 0,65%. En plus des besoins des hommes qui ne cessent d’augmenter, les écosystèmes naturels ont également besoin d’eau.

		Une sur-utilisation et une surexploitation des nappes souterraines et des cours d’eau entraînent de gros dégâts et pas seulement dans les milieux aquatiques. Avec le réchauffement planétaire tendant à réduire les précipitations, la planète risque bien de manquer d’eau d’ici peu. Economiser cette précieuse ressource doit devenir une préoccupation de chaque individu. Pour y arriver, appliquer certaines techniques devient indispensable.
		
		Cibler et colmater les fuites

		En France, les fuites constituent 30 à 40% des pertes d’eau. En la matière, les chiffres sont presque effrayants. Une chasse d’eau défaillante qui perd de l’eau peut écouler jusqu’à 300 litres quotidiennement, soit la consommation par jour d’un ménage composé de quatre personnes. Idem : un robinet qui laisse passer des gouttes d’eau peut faire perdre jusqu’à 120 litres.
		
		Pour remédier à ce problème, il existe heureusement certaines solutions. Vous pouvez par exemple installer un dispositif qui coupe automatiquement l’alimentation en eau dès qu’il détecte des anomalies dans votre consommation. Cet appareil doit être placé après le compteur. Sinon, vous pouvez réaliser le test vous-même. La nuit, avant d’aller dormir, relevez le compteur. Le matin, vérifiez celui-ci. S’il a fonctionné durant la nuit, cela signifie qu’il y a probablement une fuite quelque part.
		
		Bien entendu, pour que cette astuce fonctionne, il faut éviter de faire marcher vos robinets ou vos toilettes la nuit du test. Par ailleurs, à moins que vous ne disposiez de compétences poussées en matière de plomberie, il est préférable de faire appel à un professionnel pour qu’il détecte puis colmate la fuite.
		
		Des WC moins gourmands en eau

		On n’en a pas toujours conscience mais les toilettes constituent le deuxième poste de dépense en consommation d’eau dans une maison, d’où l’intérêt d’installer par exemple des dispositifs hydro-économes. En principe, un équipement classique consomme jusqu’à 10 litres d’eau à chaque utilisation.
		
		L’idéal est d’investir directement dans l’installation d’une chasse d’eau à double commande de 3 à 6 litres. Si vous vivez dans une maison individuelle, envisagez de mettre en place un système de récupération d’eau de pluie qui servira spécialement dans les toilettes et utilisable également pour vos travaux d’arrosage. D’autres idées sont à trouver sur Focus Maison.
		
		Solutions anti-gaspillage dans la salle de bain

		Dans la salle de bain, installer des douchettes économiques ou limiteurs de pression est une excellente idée. Ces équipements ont pour objectif de réduire la consommation en eau en ne changeant pas le confort d’utilisation lors de vos douches puisque vous bénéficiez de la même sensation même si la quantité d’eau diminue.
		
		Un système d’aspiration d’air est utilisé sur ces dispositifs, ce qui va contribuer à la variation du nombre et de la taille des gouttes d’eau. Evitez néanmoins l’usage des systèmes à effet Venturi car ils ont un impact sur la santé des poumons à cause de taille des gouttes prenant la forme de particules COV (composés organiques volatils).`
  },
  {
    id: '5',
    published: true,
    userName: 'HugoDcrq',
    userImg: 'https://api.adorable.io/avatars/233',
    title: 'Le lombricompostage, idéal en ville',
    date: '19 août 2019',
    img: 'https://www.jardinier-amateur.fr/img/reportages/p/h/photo-323-1513-l0-h0.jpeg',
    description:
      'Tout le monde connait le compost mais beaucoup moins de gens savent ce que recouvre le lombricompostage. On pourrait résumer à dire qu’il...',
    footerIcon: 'heart',
    footerText: '1,926',
    content: `Tout le monde connait le compost mais beaucoup moins de gens savent ce que recouvre le lombricompostage. On pourrait résumer à dire qu’il s’agit de la même chose à une plus petite échelle mais ce serait insuffisant et incomplet.

		Mise en place d’un lombricomposteur
		
		Le principe du lombricompostage est de produire un terreau (riche en azote, phosphore, potassium, calcium et magnésium) permettant de fertiliser votre jardin ou vos pots de fleurs à partir des déchets organiques qui auront été transformés par des vers rouges. Ces derniers se nourrissent des épluchures mises dans le lombricomposteur et vont rejeter ce qui fera ce terreau. Je vous rassure, ils rejettent beaucoup moins, en volume, que ce qu’ils ont englouti : environ 5 fois moins. Et puis, ce terreau n’a pas d’odeur, ne craignez pas les odeurs pestilentielles !
		
		Pour mettre en place votre lombricomposteur, vous avez deux solutions au choix : l’acheter ou le fabriquer vous-même. Pour l’achat, des fabricants en sont devenus spécialistes tels que Ecoworms, Lombribox ou Can-o-worms, par exemple. Ce sont d’ailleurs aussi auprès d’eux que vous vous procurerez les vers adaptés : Eisenia Fetida, Eisenia andrei ou Eisenia hortensis. Si vous êtes un tantinet bricoleur, sa fabrication n’est pas très compliquée. Vous trouverez différents tutoriels d’explication sur internet à partir de boites de polystyrène ou de bacs plastique empilables.
		
		Vous pouvez le placer sur un balcon, dans votre cave, sous un abri, dans une buanderie, voire même dans une cuisine : attention car les vers craignent le gel et les grosses chaleurs. Ils sont au mieux de leur forme entre 15 et 30C°.
		
		Quelles matières mettre dans le lombricomposteur ?

		Les vers mangent à peu près tous vos restes d’alimentation, vos épluchures de fruits et légumes, les restes de pain émietté, les coquilles d’oeuf broyées qui équilibreront le Ph, les sachets de thé, le marc de café, etc. Il est à noter quelques exceptions à ne pas mettre au lombricomposteur : la viande, l’ensemble des produits laitiers, les pâtes et le riz (moisiront et attireront les mouches), les restes d’épices et d’herbes aromatiques, les agrumes, l’ail et la rhubarbe (effet vermifuge), l’oignon et le poireau (trop acide).
		
		En dehors des déchets alimentaires, d’autres résidus peuvent être mis tels que les cheveux (riches en azote), le papier, les mouchoirs en papier, les boites d’oeufs ou encore quelques feuilles d’arbres (apport carboné).
		
		L’arrivée de mouches, le manque d’humidité, les odeurs nauséabondes,une fermentation anaérobie sont les quelques petits soucis que vous pouvez rencontrer dans les premiers temps. Mais au bout de 3 mois, vous aurez atteint votre rythme de croisière et votre lombricomposteur fonctionnera à merveille vous permettant de commencer à récupérer le terreau du premier bac. Un engrais liquide très concentré est aussi à récupérer dans la cuve du fond du lombricomposteur : il devra être dilué dans 10 volumes d’eau pour arroser vos plantes.
		
		Où que vous habitiez, quelle que soit la taille de votre foyer, faites l’essai. Et si vous avez des enfants, ce sera à la fois ludique et très instructif pour eux, en matière de recyclage et d’environnement.`
  },
  {
    id: '8',
    published: false,
    userName: 'HugoDcrq',
    userImg: 'https://api.adorable.io/avatars/233',
    title: '3 idées simple pour être écolo à la maison',
    date: '19 août 2019',
    img: 'https://reseauinternational.net/wp-content/uploads/2015/12/image-couv-site-15-1080x675.jpg',
    description:
      'On parle beaucoup de développement durable, de prendre soin de la planète et de devenir le plus écolo possible. Se sentant tous concernés, nous...',
    footerIcon: 'heart',
    footerText: '1,926',
    content: `On parle beaucoup de développement durable, de prendre soin de la planète et de devenir le plus écolo possible. Se sentant tous concernés, nous suivons assidûment les informations et actualités, mais il est aussi bon d’agir. Il suffit de quelques gestes simples dans la vie de tous les jours.

		Baisser la consommation d’énergie

		Vous pouvez commencer par apprendre à bien consommer l’eau et l’électricité à la maison. Pour cela, il faut remplacer toutes les sources de lumière en ampoules led économiques puis veiller à éteindre quand vous quittez une pièce. Il faut aussi éteindre les appareils non utilisés au lieu de les garder en veille. Ensuite, diminuez la consommation du chauffage en faisant attention à ne pas laisser entrer d’air froid, en utilisant des chiens de porte par exemple. Baissez la température du chauffage quand il n’y a personne dans la maison et prenez l’habitude de vous habiller chaudement pour ne pas mettre au maximum le thermostat. Mettez des chaussettes, des pulls et utilisez des couettes et couvertures la nuit.
		
		En ce qui concerne l’eau, adoptez les gestes écologiques. Prenez soin de fermer le robinet quand vous vous lavez les dents et les mains puis favorisez la douche à la place du bain. Dans la cuisine, ne laissez pas couler l’eau pendant la vaisselle et réutilisez les eaux usagées, par exemple après le lavage des légumes, pour arroser les plantes. Lorsque vous lavez votre voiture, munissez-vous d’un seau et d’une cuvette à la place du tuyau d’arrosage.
		
		Utiliser des produits nettoyants bio
		
		Il est très facile de fabriquer soi-même les produits nettoyants. Voici la recette du liquide vaisselle maison : dans 350 ml d’eau, ajoutez 100 ml de savon de Marseille en liquide puis 1 cuillerée à café de bicarbonate de soude. Bien mélanger le tout et pour parfumer, versez 15 à 20 gouttes d’huile essentielle de citron. Ensuite pour la lessive, il vous faut 30 g de savon de Marseille râpé que vous diluez dans 1 l d’eau bouillante à laquelle vous rajoutez 2 cuillerées à soupe de bicarbonate de soude.
		
		Lorsque le mélange a refroidi, mettez 20 gouttes d’huile essentielle de lavande. Enfin, pour les détergents sols, il vous suffit de mélanger 500 ml d’eau à 100 ml de vinaigre blanc puis 100 g de bicarbonate de soude et finalisez par 3 cuillerées à soupe de savon noir liquide. En utilisant ces trois produits à la maison, vous verrez qu’en plus d’être écolo, vous ferez des économies.
		
		Trier ses déchets
		
		Limiter les déchets est aussi un très bon geste pour la planète. Faites donc un tri sélectif dans votre poubelle et respectez les bacs à l’extérieur lorsque vous videz vos poubelles. Quand vous faites vos courses, munissez-vous d’un cabas réutilisable et choisissez les produits avec moins d’emballage. Achetez également en vrac pour consommer selon vos besoins et optez pour des produits bio.
		
		Appliquer ces trois principes c’est agir réellement pour la préservation de la planète, le plus difficile est de commencer mais en adoptant ces gestes au quotidien, ils deviendront vite une habitude.`
  }
];
