import React, {Component} from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Left,
  Right,
  Body,
  Text,
} from 'native-base';

class FeedsPage extends Component {
  static navigationOptions = {
    title: 'Feeds',
  };

  render() {
    // const {navigate} = this.props.navigation;
    return (
      <Container>
        <Header>
          <Left>
            <Title>Feeds</Title>
          </Left>
          <Body />
          <Right />
        </Header>
        <Content>
          <Text>This is Content Section of Feeds Page</Text>
        </Content>
      </Container>
    );
  }
}

export default FeedsPage;
