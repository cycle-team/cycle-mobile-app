import React, {Component} from 'react';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Text,
  Tabs,
  Tab,
  TabHeading,
} from 'native-base';
import ProfilHeader from '../components/profil-header';
import ProfileFeedList from '../components/profil-feed-list';
import ProfilAbout from '../components/profil-about';

class ProfilePage extends Component {
  static navigationOptions = {
    title: 'Profile',
  };

  render() {
    const {navigate} = this.props.navigation;
    return (
      <Container>
        <Header>
          <Left style={{marginLeft: 5}}>
            <Title>Profile</Title>
          </Left>
          <Body></Body>
          <Right style={{marginRight: 5}}>
            <Button transparent>
              <Icon name="circle-edit-outline" type="MaterialCommunityIcons" />
            </Button>
          </Right>
        </Header>

        <ProfilHeader />

        <Tabs>
          <Tab
            heading={
              <TabHeading>
                <Icon name="bars" type="AntDesign" style={{fontSize: 20}} />
                <Text>Mon activité</Text>
              </TabHeading>
            }>
            <Content>
              <ProfileFeedList />
            </Content>
          </Tab>

          <Tab
            heading={
              <TabHeading>
                <Icon
                  name="information-variant"
                  type="MaterialCommunityIcons"
                  style={{fontSize: 20}}
                />
                <Text>Mes informations</Text>
              </TabHeading>
            }>
            <Content>
              <ProfilAbout></ProfilAbout>
            </Content>
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

export default ProfilePage;
