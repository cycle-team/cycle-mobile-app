import React, {Component} from 'react';
import {
  List,
  ListItem,
  Button,
  Left,
  Right,
  Body,
  Text,
  Thumbnail,
} from 'native-base';

export default class ProfileFeedList extends Component {
  render() {
    return (
      <List>
        <ListItem thumbnail>
          <Left>
            <Thumbnail source={{uri: 'https://api.adorable.io/avatars/233'}} />
          </Left>
          <Body>
            <Text>HugoDcqr a publié un article</Text>
            <Text note numberOfLines={1}>
              18 août 2019
            </Text>
            <Text note>
              L’eau n’est pas inépuisable. Eviter de la gaspiller est primordial
              voire vital. A savoir déjà que sur la planète, plus de 90% de
              l’eau est...
            </Text>
          </Body>
          <Right>
            <Button transparent>
              <Text>View</Text>
            </Button>
          </Right>
        </ListItem>
      </List>
    );
  }
}
