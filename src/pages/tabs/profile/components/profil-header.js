import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {Card, Thumbnail, CardItem, Text, H2} from 'native-base';

export default class ProfilHeader extends Component {
  render() {
    return (
      <Card transparent style={styles.container}>
        <CardItem style={styles.item}>
          <H2>HugoDcrq</H2>
          <Text note>Niveau 2</Text>
        </CardItem>
        <CardItem style={styles.item}>
          <Thumbnail
            large
            source={{
              uri: 'https://api.adorable.io/avatars/233',
            }}
          />
        </CardItem>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  item: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
});
