import React, {Component} from 'react';
import {
  List,
  ListItem,
  Button,
  Left,
  Right,
  Body,
  Text,
  Thumbnail,
} from 'native-base';

export default class ProfilAbout extends Component {
  render() {
    return (
      <List>
        <ListItem itemHeader first style={{marginBottom: -20}}>
          <Text>DONNEES PERSO</Text>
        </ListItem>
        <ListItem>
          <Text>Nom: </Text>
          <Text note>Hugo</Text>
        </ListItem>
        <ListItem>
          <Text>Genre: </Text>
          <Text note>Masculin</Text>
        </ListItem>
        <ListItem last>
          <Text>Âge: </Text>
          <Text note>23 ans</Text>
        </ListItem>
        <ListItem itemHeader style={{marginTop: 20, marginBottom: -20}}>
          <Text>GROUPES</Text>
        </ListItem>
        <ListItem thumbnail last>
          <Left>
            <Thumbnail
              square
              source={{
                uri:
                  'https://melenchon.fr/wp-content/uploads/2016/11/greenpeace.jpg',
              }}
            />
          </Left>
          <Body>
            <Text>greenpeace</Text>
            <Text note numberOfLines={1}>
              Greenpeace est une ONGI de protection de l'environnement présente
              dans plus de 55 pays à travers le monde1.
            </Text>
          </Body>
          <Right>
            <Button transparent>
              <Text>View</Text>
            </Button>
          </Right>
        </ListItem>
        <ListItem itemHeader style={{marginTop: 20, marginBottom: -20}}>
          <Text>BADGES</Text>
        </ListItem>
        <ListItem last>
          <Text>Hugo n'a encore aucun badge.</Text>
        </ListItem>
        <ListItem itemHeader style={{marginTop: 20, marginBottom: -20}}>
          <Text>REALISATIONS</Text>
        </ListItem>
        <ListItem>
          <Text>Hugo n'a réalisé aucun défi.</Text>
        </ListItem>
      </List>
    );
  }
}
