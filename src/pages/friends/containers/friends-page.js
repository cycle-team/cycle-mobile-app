import React, {Component} from 'react';
import {ProgressBarAndroid} from 'react-native';
import {
  Container,
  Header,
  Title,
  Icon,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
} from 'native-base';

import {friends} from '../friends-mock';

export default class FriendsPage extends Component {
  state = {
    friends: friends,
  };

  render() {
    return (
      <Container>
        <Header>
          <Left style={{marginLeft: 5}}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Mes amis</Title>
          </Body>
          <Right style={{marginRight: 5}}>
            <Button transparent>
              <Icon name="account-search" type="MaterialCommunityIcons" />
            </Button>
            <Button transparent>
              <Icon name="account-plus" type="MaterialCommunityIcons" />
            </Button>
          </Right>
        </Header>
        <Content>
          <List>
            {this.state.friends.map((friend, index) => {
              return (
                <ListItem thumbnail key={index}>
                  <Left>
                    <Thumbnail square source={{uri: friend.userImg}} />
                  </Left>
                  <Body>
                    <Text>{friend.userName}</Text>
                    <Text note numberOfLines={1}>
                      Niveau {friend.level}
                    </Text>
                    <ProgressBarAndroid
                      styleAttr="Horizontal"
                      color="#91C9AA"
                      indeterminate={false}
                      progress={friend.levelProgress}
                      style={{width: '40%'}}
                    />
                  </Body>
                  <Right>
                    <Button transparent>
                      <Text>Voir</Text>
                    </Button>
                  </Right>
                </ListItem>
              );
            })}
          </List>
        </Content>
      </Container>
    );
  }
}
