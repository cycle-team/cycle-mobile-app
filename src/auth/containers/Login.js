import React, {Component} from 'react';
// import firebase from 'react-native-firebase';
import {
  View,
  Text,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  Keyboard,
  Image,
} from 'react-native';
import colors from '../styles/color';
import InputField from '../components/form/InputField';
import NextArrowButton from '../components/buttons/NextArrowButton';

import {NavigationActions} from 'react-navigation';
import BackArrowButton from '../components/buttons/BackArrowButton';

import {login, saveUserToken} from '../auth-service';

export default class Login extends Component {
  constructor() {
    super();

    this.state = {
      user: null,
      email: '',
      password: '',
      showButton: true,
      token: '',
      loader: false,
      error: false,
    };
  }

  goBack = () => {
    const navigateAction = NavigationActions.navigate({
      routeName: 'Auth',
    });

    this.props.navigation.dispatch(navigateAction);
  };

  componentDidMount() {
    /**
    this.unsubscriber = firebase.auth().onAuthStateChanged(user => {
      this.setState({ user });
    });
     */

    Keyboard.addListener('keyboardDidShow', this.keyboardWillShow);
    Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);
  }

  handleSubmitLogin = () => {
    this.setState({loader: true});

    login(this.state.email, this.state.password)
      .then(response => response.text())
      .then(token => {
        if (token) {
          saveUserToken(token).then(
            () => {
              this.setState({loader: false});
              this.props.navigation.navigate('App', {});
            },
            () => {
              this.setState({loader: false, error: true});
            },
          );
        }
      })
      .catch(error => {
        this.setState({loader: false, error: true});
      });
  };

  componentWillUnmount() {
    if (this.unsubscriber) {
      this.unsubscriber();
    }
    Keyboard.removeListener('keyboardDidShow', this.keyboardWillShow);
    Keyboard.removeListener('keyboardDidHide', this.keyboardWillHide);
  }

  keyboardWillShow = () => {
    this.setState({showButton: false});
  };

  keyboardWillHide = () => {
    this.setState({showButton: true});
  };

  handleEmailChange = email => {
    // parent class change handler is always called with field name and value
    this.setState({email: email});
  };

  handlePasswordChange = password => {
    // parent class change handler is always called with field name and value
    this.setState({password: password});
  };

  render() {
    return (
      <KeyboardAvoidingView style={styles.wrapper} behavior="padding">
        <View style={styles.scrollViewWrapper}>
          <ScrollView style={styles.scrollView}>
            <Text style={styles.loginHeader}>Connexion</Text>
            <InputField
              labelText="EMAIL ADDRESS OR USERNAME"
              onChangeText={this.handleEmailChange}
              labelTextSize={14}
              labelColor={colors.white}
              textColor={colors.white}
              borderBottomColor={colors.white}
              inputType="email"
              customStyle={{marginBottom: 30}}
            />
            <InputField
              labelText="PASSWORD"
              onChangeText={this.handlePasswordChange}
              labelTextSize={14}
              labelColor={colors.white}
              textColor={colors.white}
              borderBottomColor={colors.white}
              inputType="password"
              customStyle={{marginBottom: 30}}
            />
          </ScrollView>

          {this.state.loader ? (
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'center',
                marginBottom: 120,
              }}>
              <Image
                source={require('../img/whiteLoader.gif')}
                style={{width: 120, height: 60}}
              />
            </View>
          ) : null}

          {!this.state.loader && this.state.error ? (
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'center',
                marginBottom: 120,
              }}>
              <Text style={{color: 'white', fontWeight: '600'}}>
                Erreur: Veuillez verifier vos identifiants et réssayer
              </Text>
            </View>
          ) : null}

          <Text>{this.state.token}</Text>

          {this.state.showButton ? (
            <View style={styles.buttonWrapper}>
              <BackArrowButton handleBack={this.goBack} />
              <NextArrowButton handleLogin={this.handleSubmitLogin} />
            </View>
          ) : null}
        </View>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  buttonWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    bottom: 20,
    marginHorizontal: 20,
  },
  wrapper: {
    display: 'flex',
    flex: 1,
    backgroundColor: colors.green01,
    paddingTop: 30,
  },
  scrollView: {
    paddingLeft: 30,
    paddingRight: 30,
    flex: 1,
  },
  scrollViewWrapper: {
    marginTop: 100,
    flex: 1,
  },
  loginHeader: {
    fontSize: 28,
    color: colors.white,
    fontWeight: '300',
    marginBottom: 40,
  },
});
