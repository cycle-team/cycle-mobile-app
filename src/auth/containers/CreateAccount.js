import React, {Component} from 'react';
// import firebase from 'react-native-firebase';
import {
  View,
  Text,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  Keyboard,
} from 'react-native';
import colors from '../styles/color';
import InputField from '../components/form/InputField';
import NextArrowButton from '../components/buttons/NextArrowButton';

import {NavigationActions} from 'react-navigation';
import BackArrowButton from '../components/buttons/BackArrowButton';

import {createAccount} from '../auth-service';

export default class CreateAccount extends Component {
  constructor() {
    super();

    this.state = {
      user: null,
      email: '',
      password: '',
      showButton: true,
      loader: false,
      create: false,
    };
  }
  static navigationOptions = {
    headerTintColor: colors.green01,
    headerStyle: {
      backgroundColor: colors.green01,
    },
  };

  goBack = () => {
    const navigateAction = NavigationActions.navigate({
      routeName: 'Auth',
    });

    this.props.navigation.dispatch(navigateAction);
  };

  componentDidMount() {
    /**
    this.unsubscriber = firebase.auth().onAuthStateChanged(user => {
      this.setState({ user });
    });
     */

    Keyboard.addListener('keyboardDidShow', this.keyboardWillShow);
    Keyboard.addListener('keyboardDidHide', this.keyboardWillHide);
  }

  handleSubmitCreateAccount = () => {
    this.setState({loader: true});

    createAccount(this.state.email, this.state.password)
      .then(response => response.status)
      .then(status => {
        if (status === 201) {
          this.setState({loader: false, create: true});
        }
      })
      .catch(error => {
        this.setState({loader: false, error: true});
      });
  };

  componentWillUnmount() {
    if (this.unsubscriber) {
      this.unsubscriber();
    }
    Keyboard.removeListener('keyboardDidShow', this.keyboardWillShow);
    Keyboard.removeListener('keyboardDidHide', this.keyboardWillHide);
  }

  keyboardWillShow = () => {
    this.setState({showButton: false});
  };

  keyboardWillHide = () => {
    this.setState({showButton: true});
  };

  handleEmailChange = email => {
    // parent class change handler is always called with field name and value
    this.setState({email: email});
  };

  handlePasswordChange = password => {
    // parent class change handler is always called with field name and value
    this.setState({password: password});
  };

  render() {
    return (
      <KeyboardAvoidingView style={styles.wrapper} behavior="padding">
        <View style={styles.scrollViewWrapper}>
          <ScrollView style={styles.scrollView}>
            <Text style={styles.loginHeader}>Créer votre compte</Text>
            <InputField
              labelText="EMAIL ADDRESS OR USERNAME"
              onChangeText={this.handleEmailChange}
              labelTextSize={14}
              labelColor={colors.white}
              textColor={colors.white}
              borderBottomColor={colors.white}
              inputType="email"
              customStyle={{marginBottom: 30}}
            />
            <InputField
              labelText="PASSWORD"
              onChangeText={this.handlePasswordChange}
              labelTextSize={14}
              labelColor={colors.white}
              textColor={colors.white}
              borderBottomColor={colors.white}
              inputType="password"
              customStyle={{marginBottom: 30}}
            />
            <InputField
              labelText="CONFIRM PASSWORD"
              onChangeText={this.handlePasswordChange}
              labelTextSize={14}
              labelColor={colors.white}
              textColor={colors.white}
              borderBottomColor={colors.white}
              inputType="password-confirm"
              customStyle={{marginBottom: 30}}
            />
          </ScrollView>

          {this.state.create ? (
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'center',
                marginBottom: 120,
              }}>
              <Text style={{color: 'white', fontWeight: '600'}}>
                Compte créer
              </Text>
            </View>
          ) : null}

          {this.state.showButton ? (
            <View style={styles.buttonWrapper}>
              <BackArrowButton handleBack={this.goBack} />
              <NextArrowButton handleLogin={this.handleSubmitCreateAccount} />
            </View>
          ) : null}
        </View>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  buttonWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    bottom: 20,
    marginHorizontal: 20,
  },
  wrapper: {
    display: 'flex',
    flex: 1,
    backgroundColor: colors.green01,
  },
  scrollView: {
    paddingLeft: 30,
    paddingRight: 30,
    flex: 1,
  },
  scrollViewWrapper: {
    marginTop: 70,
    flex: 1,
  },
  loginHeader: {
    fontSize: 28,
    color: colors.white,
    fontWeight: '300',
    marginBottom: 40,
  },
});
