/**
 * Airbnb Clone App
 * @author: Andy
 * @Url: https://www.cubui.com
 */

import React, {Component} from 'react';
import {PropTypes} from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import {TouchableHighlight, StyleSheet} from 'react-native';
import colors from '../../styles/color';

export default class BackArrowButton extends Component {
  render() {
    const {disabled, handleBack} = this.props;
    // console.log()
    return (
      <TouchableHighlight
        onPress={handleBack}
        style={[{opacity: 0.6}, styles.button]}>
        <Icon
          name="angle-left"
          color={colors.green01}
          size={32}
          style={styles.icon}
        />
      </TouchableHighlight>
    );
  }
}

BackArrowButton.propTypes = {
  disabled: PropTypes.bool,
  handleNextButton: PropTypes.func,
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    width: 60,
    height: 60,
    backgroundColor: colors.white,
  },
  icon: {
    marginRight: 2,
    marginTop: 2,
  },
});
