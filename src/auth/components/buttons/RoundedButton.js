import React, {Component} from 'react';
import propTypes from 'prop-types';
import {Text, View, TouchableHighlight, StyleSheet} from 'react-native';
import colors from '../../styles/color';
import {Icon} from 'native-base';

export default class RoundedButton extends Component {
  render() {
    const {text, color, icon, backgroundColor} = this.props;
    return (
      <TouchableHighlight
        style={[{backgroundColor}, styles.wrapper]}
        onPress={this.props.click}>
        <View style={styles.ButtonTextWrapper}>
          <Icon
            name={icon}
            type="MaterialCommunityIcons"
            size={20}
            style={{color: color}}
          />
          <Text style={[{color}, styles.buttonText]}>{text}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}
RoundedButton.propTypes = {
  text: propTypes.string.isRequired,
  color: propTypes.string,
  backgroundColor: propTypes.string,
};
const styles = StyleSheet.create({
  wrapper: {
    padding: 15,
    display: 'flex',
    borderRadius: 40,
    borderWidth: 1,
    borderColor: colors.white,
    marginBottom: 15,
    alignItems: 'center',
  },
  ButtonTextWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginLeft: 20,
  },
  buttonText: {
    fontSize: 16,
    width: '100%',
    textAlign: 'center',
    marginLeft: -20,
  },
});
