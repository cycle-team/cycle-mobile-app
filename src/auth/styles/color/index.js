export default {
  black: '#000000',
  white: '#ffffff',
  green01: '#91C9AA',
  termsTextSize: 16,
};
