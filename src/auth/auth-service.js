import AsyncStorage from '@react-native-community/async-storage';
import config from '../config';

export const login = (username, password) => {
  return fetch(`${config.apiUrlBackend}/auth/login`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      username: username,
      password: password,
    }),
  });
};

export const createAccount = (username, password) => {
  return fetch(`${config.apiUrlBackend}/user`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      username: username,
      password: password,
      role: 'USER',
    }),
  });
};

export const saveUserToken = async userToken => {
  try {
    await AsyncStorage.setItem('user_token', userToken);
  } catch (error) {
    // Error retrieving data
    console.log(error.message);
  }
};
