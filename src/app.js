import React from 'react';

// Navigation
import {createAppContainer} from 'react-navigation';
import * as navigation from './navigation';

// Theme
import {StyleProvider} from 'native-base';
import getTheme from '/native-base-theme/components';
import material from '/native-base-theme/variables/material';

let Navigation = createAppContainer(navigation.AppNavigator);

export default class App extends React.Component {
  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <Navigation />
      </StyleProvider>
    );
  }
}
